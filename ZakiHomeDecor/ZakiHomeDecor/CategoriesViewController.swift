//
//  CategoriesViewController.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 7/28/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit
import Alamofire

class CategoriesViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tblView: UITableView!
    var arrCat = [Dictionary<String, Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
            self.getCategoriesList(strToken: access_token)
        }
        self.title = "CATEGORIES"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - categories list call
    func getCategoriesList(strToken:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + strToken
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/categories", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let httpStatusCode = response.response?.statusCode {
                        print("httpStatusCode is... \(httpStatusCode)")
                        print(response.result.value as Any)
                        if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                        {
                            if let arrData = dictData["data"] as! NSArray? {
                                for obj in arrData {
                                    self.arrCat.append(obj as! [String : Any])
                                }
                            }
                        }
                        self.tblView.reloadData()
                        self.stopActivityIndicator()
                        
                    }
                }
                break
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrCat.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell", for: indexPath) as! CategoriesTableViewCell
        cell.selectionStyle = .none
        if arrCat.count > 0 {
            let objDoc = arrCat[indexPath.section]
            let strDoctName = objDoc["name"]!
            cell.lblCategoryName.text = "\(strDoctName)"
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objDoc = arrCat[indexPath.section]
        let strCatID = objDoc["category_id"]!
        let strCatName = objDoc["name"]!
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductsByCategoryViewController") as! ProductsByCategoryViewController
        vc.cat_id = "\(strCatID)"
        vc.CatName = "\(strCatName)"
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
