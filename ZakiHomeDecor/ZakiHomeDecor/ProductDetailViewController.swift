//
//  ProductDetailViewController.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 8/11/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class ProductDetailViewController: UIViewController,UIScrollViewDelegate {
    
    var arrProductDetail = [Dictionary<String,AnyObject>]()
    var scrollview_pagecontrol: UIScrollView?
    
    @IBOutlet weak var mainViewHight: NSLayoutConstraint!
    @IBOutlet var view_back_scroll: UIView?
    @IBOutlet var mainView: UIView?
    @IBOutlet  var pageControl: UIPageControl?
    var arrProductImg = NSMutableArray()
    var product_id:String!
    var dictCart = [String:String]()
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductDesc: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblAvailibility: UILabel!
    @IBOutlet weak var textQyantity: UITextField!

    var galleryImgArr = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "PRODUCT DETAIL"
        // Do any additional setup after loading the view.
        if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
            self.getProductFromID(strProductId: self.product_id!, strToken: access_token)
        }
       
         //self.createPageControl()
        print(UserDefaults.standard.bool(forKey: "productsInCart"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Product from ID
    func getProductFromID(strProductId:Any,strToken:String) {
        self.arrProductImg.removeAllObjects()
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + strToken
        reqheader["Content-Type"] = "application/json"
        let strURL = "http://zakihomedecor.com/api/rest/products/" + "\(strProductId)"
        print(strURL)
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let httpStatusCode = response.response?.statusCode {
                        print("httpStatusCode is... \(httpStatusCode)")
                        print(response.result.value as Any)
                        if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                        {
                            if let arrData = dictData["data"] as! NSDictionary?
                            {
                                self.lblProductName.text = arrData["name"] as? String
                                
                                if let priceGet = dictData["price"] {
                                    let priceString = "RS. "
                                    let price = priceString + "\(priceGet)"
                                    
                                    self.lblProductPrice.text = price
                                }
                                let htmlString = arrData["description"] as? String
                                // works even without <html><body> </body></html> tags, BTW
                                let data = htmlString?.data(using: String.Encoding.unicode)! // mind "!"
                                let attrStr = try? NSAttributedString( // do catch
                                    data: data!,
                                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                    documentAttributes: nil)
                                // suppose we have an UILabel, but any element with NSAttributedString will do
                                self.lblProductDesc.attributedText = attrStr
                                self.lblDescription.attributedText = attrStr
//                                self.lblProductDesc.text = arrData["description"] as? String
//                                self.lblDescription.text = arrData["description"] as? String
                                
                                if let decrip = arrData["description"] as? String
                                {
                                    if decrip == ""{
                                        self.lblDescription.text = "Description is not available"
                                        self.mainViewHight.constant = self.view.frame.size.height + 20
                                        self.mainView?.layoutIfNeeded()
                                        
                                        if self.view.frame.size.height < 580 {
                                            
                                            self.mainViewHight.constant = 600
                                            self.mainView?.layoutIfNeeded()
                                        }
                                        
                                    }else{
                                        
                                        self.mainViewHight.constant = 800
                                        self.mainView?.layoutIfNeeded()
                                    }
                                    self.lblProductDesc.text = "\(decrip)"
                                }
                                
                                if let galleryImg = arrData["images"] as! NSArray?
                                {
                                    
                                    if(galleryImg.count > 0)
                                    {
                                        for obj in galleryImg
                                        {
                                            self.arrProductImg.add(obj)
//                                            let dict = galleryImg[String(i)]
//                                            self.arrProductImg.add(dict!)
                                            
                                        }
                                    }
                                    else {
                                        
                                        if let img1 = arrData["image"] as? String {
                                            self.arrProductImg.add(img1)
                                            
                                        }
                                        if let img2 = arrData["original_image"] as? String {
                                            self.arrProductImg.add(img2)
                                            
                                        }
                                    }
                                    
                                    
                                }
                                if let galleryImg1 = arrData["original_images"] as! NSArray?
                                {
                                    if(galleryImg1.count > 0)
                                    {
                                        for obj in galleryImg1
                                        {
                                            self.arrProductImg.add(obj)
                                            
                                        }
                                    }
                                }
                                
                                self.createPageControl()
                                
                            }
                        }
                        self.stopActivityIndicator()
                        
                    }
                }
                break
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    
    //MARK: -  Page Control
    func createPageControl() {
        if self.arrProductImg.count <= 0 {
           self.arrProductImg = ["http://zakihomedecor.com/image/catalog/bath%20tub/Picture1.jpg","http://zakihomedecor.com/image/catalog/bath%20tub/Picture2.jpg","http://zakihomedecor.com/image/catalog/bath%20tub/Picture3.jpg"]
        }
        scrollview_pagecontrol = UIScrollView(frame: CGRect(x: CGFloat((view_back_scroll?.frame.origin.x)!), y: 0, width: CGFloat((view_back_scroll?.frame.size.width)!), height: CGFloat((self.view_back_scroll?.frame.size.height)!)))
        scrollview_pagecontrol?.showsHorizontalScrollIndicator = false
        scrollview_pagecontrol?.delegate = self
        scrollview_pagecontrol?.bounces = false
        scrollview_pagecontrol?.tag = 1
        self.view_back_scroll?.addSubview(scrollview_pagecontrol!)
        pageControl?.pageIndicatorTintColor = UIColor.lightGray
        pageControl?.numberOfPages = arrProductImg.count
        pageControl?.currentPage = 0
        pageControl?.currentPageIndicatorTintColor = UIColor.blue
        pageControl?.tag = 12

        for i in 0..<arrProductImg.count {
            //We'll create an imageView object in every 'page' of our scrollView.
            var frame = CGRect.zero
            frame.origin.x = view.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size = view.frame.size
            frame.size.height = (scrollview_pagecontrol?.frame.size.height)!
            let imageView = UIImageView(frame: frame)
            let images: String? = (arrProductImg[i] as? String)
            DispatchQueue.main.async {
                let imgURL = URL(string:images!)
                imageView.kf.setImage(with:imgURL)
            }
            
            scrollview_pagecontrol?.addSubview(imageView)
            scrollview_pagecontrol?.isPagingEnabled = true
            
            
            scrollview_pagecontrol?.contentSize = CGSize(width: CGFloat((scrollview_pagecontrol?.frame.size.width)! * CGFloat( arrProductImg.count)), height: CGFloat((scrollview_pagecontrol?.frame.size.height)!))
            
        }
    }
    
    // MARK: - UIScrollView Delegate
    func scrollViewDidScroll(_ sender: UIScrollView) {

        let pageWidth: CGFloat = scrollview_pagecontrol!.frame.size.width
        let Page: CGFloat = floor((scrollview_pagecontrol!.contentOffset.x - CGFloat(pageWidth / 2)) / CGFloat(pageWidth)) + CGFloat(1)
        pageControl?.currentPage = Int(Page)
    }
    
    //MARK: - Add product to cart API call
    func addProductToCart(dictCart: [String:String],token:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + token
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/cart", method: .post, parameters: dictCart, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                    {
                        
                        if let success = dictData["success"]
                        {
                            
                            if success.boolValue {
                                //total_product_count
                                //check for cart products
                                if let arrData = dictData["data"] as! Dictionary<String,AnyObject>?
                                {
                                    let cartCount = arrData["total_product_count"]
                                    UserDefaults.standard.set(cartCount!, forKey: "cartCount")
                                }
                                
                                UserDefaults.standard.set(true, forKey: "productsInCart")
                                self.displayAlertWithMessage(viewController: self, title: "", message: "Product added to cart.")

                            }
                            else {
                                 UserDefaults.standard.set(false, forKey: "productsInCart")
                            }
                            
                        }
                        
                    }
                    self.stopActivityIndicator()
                }
                break
                
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 400:
                        message = "Bad Request"
                    case 409:
                        message = "Conflict, Email Id is already registered"
                    case 500:
                        message = "Server Error!"
                    default:
                        message = (response.result.error?.localizedDescription)!
                    }
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: message)
                }
                else {
                    message = (response.result.error?.localizedDescription)!
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: "Error in server connection!")
                }
                break
                
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnAddToCartPressed(_ sender: Any) {
        let strQuantity = textQyantity.text
        dictCart["product_id"] = self.product_id!
        dictCart["quantity"] = strQuantity
        if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
            self.addProductToCart(dictCart: dictCart, token: access_token)
        }
        
    }
    
    @IBAction func btnWislistPressed(_ sender: Any) {
    }
}
