//
//  ForgotPasswordViewController.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 8/5/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var textEmail: UITextField!
    var dictLogin = [String:String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - customer's registration API call
    func userForgotPassword(dictRegister: [String:String],token:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + token
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/forgotten", method: .post, parameters: dictRegister, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                    {
                        self.stopActivityIndicator()
                        if let success = dictData["success"]
                        {
                            
                            if success.boolValue {
                                
                            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Password sent to the email")
                            }
                            else{
                             self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Warning: The E-Mail Address was not found in our records, please try again!")
                            }
                        }
                        
                    }
                }
                break
                
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 400:
                        message = "Bad Request"
                    case 409:
                        message = "Conflict, Email Id is already registered"
                    case 500:
                        message = "Server Error!"
                    default:
                        message = (response.result.error?.localizedDescription)!
                    }
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: message)
                }
                else {
                    message = (response.result.error?.localizedDescription)!
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: "Error in server connection!")
                }
                break
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func submitPressed(_ sender: Any) {
        let strEmail = textEmail.text
        
        //Validation
        if !isValidEmail(testStr: strEmail!) == true {
            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Please enter a valid email Id!")
        }
        else {
            dictLogin["email"] = strEmail
            if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                self.userForgotPassword(dictRegister: dictLogin, token: access_token)
            }
            
        }
    }
    
    //MARK: - UTIL
    //Validate Email
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
