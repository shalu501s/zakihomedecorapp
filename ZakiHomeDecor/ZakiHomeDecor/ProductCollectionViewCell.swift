//
//  ProductCollectionViewCell.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 8/7/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    
}
