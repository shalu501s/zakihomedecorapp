//
//  RegistrationViewController.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 7/28/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit
import Alamofire
import ActionSheetPicker_3_0

class RegistrationViewController: UIViewController {
    //Outlets
    @IBOutlet weak var text_firstname: UITextField!
    @IBOutlet weak var text_lastname: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var text_telephone: UITextField!
    @IBOutlet weak var text_Password: UITextField!
    @IBOutlet weak var text_ConfirmPassword: UITextField!
    @IBOutlet weak var text_Address: UITextField!
    @IBOutlet weak var text_City: UITextField!
    @IBOutlet weak var text_Pin: UITextField!
    @IBOutlet weak var text_State: UITextField!
    
    //Variables
    var dictRegistration = [String:String]()
    var strState:String!
    var strZoneID:String!
    var strToken:String!
    var arrStates = [String]()
    var arrZoneID = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "REGISTARTION"
        self.navigationController?.navigationBar.isHidden = true
        if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
            self.getStatesList(strToken: access_token)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - customer's registration API call
    func userRegistration(dictRegister: [String:String],token:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + token
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/register", method: .post, parameters: dictRegister, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                    {
                        
                        if let success = dictData["success"]
                        {
                            
                            if success.boolValue {
                                
                                let data = dictData["data"] as! NSDictionary
                                UserDefaults.standard.setValue(data["customer_id"]!, forKey: "user_id")//customer_id
                                self.stopActivityIndicator()
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                        }
                        
                    }
                }
                break
                
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 400:
                        message = "Bad Request"
                    case 409:
                        message = "Conflict, Email Id is already registered"
                    case 500:
                        message = "Server Error!"
                    default:
                        message = (response.result.error?.localizedDescription)!
                    }
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: message)
                }
                else {
                    message = (response.result.error?.localizedDescription)!
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: "Error in server connection!")
                }
                break
                
            }
        }
    }

    //MARK: - Get states
    func getStatesList(strToken:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + strToken
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/countries/99", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let httpStatusCode = response.response?.statusCode {
                        print("httpStatusCode is... \(httpStatusCode)")
                        print(response.result.value as Any)
                        if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                        {
                            if let arrData = dictData["data"] as! NSDictionary?
                             {
                                if let arrStates = arrData["zone"] as! NSArray? {
                                    for obj in arrStates {
                                        let objStates = obj as! Dictionary<String,AnyObject>
                                        self.arrStates.append(objStates["name"] as! String)
                                        self.arrZoneID.append(objStates["zone_id"] as! String)
                                        
                                    }
                                }
//                                let arrtempzone = tempArr as! NSArray
//                                let objState = arrtempzone[""]
//                                for obj in tempArr {
//                                    if let strStateName = obj["name"] as! String {
//                                       self.arrStates.append(strStateName)
//                                    }
//
//                                }
                                
                            }
                        }
                        self.stopActivityIndicator()
                        
                    }
                }
                break
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnGoToLoginPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func selectStatePressed(_ sender: Any) {
        var arrTemp = ["Assaam", "Delhi", "Madhya Pradesh", "Gujarat", "Maharashtra"]
        if arrStates.count > 0 {
            arrTemp = self.arrStates
        }
        ActionSheetStringPicker.show(withTitle: "Please Select State", rows: arrTemp, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            self.strState = "\(index!)"
            self.strZoneID = self.arrZoneID[value]
            self.text_State.text = self.strState
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func signUpPressed(_ sender: Any) {

        let firstname = text_firstname.text//
        let lastname = text_lastname.text//
        let email = textEmail.text//
        let telephone = text_telephone.text//
        let address_1 = text_Address.text//
        let agree = "1"//
        let country_id = "1"//
        let postcode = text_Pin.text//
        var zone_id = "1478"
        if arrZoneID.count > 0 {
          zone_id = self.strZoneID
        }
        let confirm = text_ConfirmPassword?.text//
        let city = text_City.text//
        let password = text_Password.text
        
        //Validation
        if !isValidEmail(testStr: email!) == true {
            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Please enter a valid email Id!")
        }
        else if (firstname?.utf8CString.count)! <= 2 {
            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Please enter a first name!")
        }
        else if (telephone?.utf8CString.count)! <= 0 {
            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Please enter your phone number")
        }
        else if (password?.utf8CString.count)! <= 4 {
            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Password must be between 4 and 20 characters!")
        }
        else if !(confirm?.utf8CString == password?.utf8CString) {
            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Password does not match!")
        }
        else {
            if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                strToken = access_token
            }
            dictRegistration["firstname"] = firstname
            dictRegistration["lastname"] = lastname
            dictRegistration["email"] = email
            dictRegistration["password"] = password
            dictRegistration["telephone"] = telephone
            dictRegistration["agree"] = agree
            dictRegistration["confirm"] = confirm
            dictRegistration["city"] = city
            dictRegistration["address_1"] = address_1
            dictRegistration["zone_id"] = zone_id
            dictRegistration["country_id"] = country_id
            dictRegistration["postcode"] = postcode
            self.userRegistration(dictRegister: dictRegistration, token: strToken)
        }
    }
    //MARK: - UTIL
    //Validate Email
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
