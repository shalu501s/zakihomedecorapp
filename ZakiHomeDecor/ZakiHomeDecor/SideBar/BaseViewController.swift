//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import Alamofire

class BaseViewController: UIViewController, SlideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            print("Sign In\n", terminator: "")
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
//            if let loginSstatus = UserDefaults.standard.object(forKey:"Login_Status"){
//            if loginSstatus as! Bool == true
//            {
//
//                let alert = UIAlertController(title: "Loged In", message: "You are already loged in", preferredStyle: UIAlertControllerStyle.alert)
//                
//                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//                
//                self.present(alert, animated: true, completion: nil)
//                
//            }else{
//                
//                let storyboard = UIStoryboard(name:"Main", bundle: nil)
//                let vc  = storyboard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//            }

            
            break
//        case 1:
//            print("Wishlist\n", terminator: "")
//
////            UserDefaults.standard.setValue("CATEGORIES",forKey:"Catagoires_title")
////            self.openViewControllerBasedOnIdentifier("HomeViewController")
//
//            break
        case 1:
            print("About Us\n", terminator: "")
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier:"AboutUsViewController") as! AboutUsViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            break
       
//        case 3:
//            print("My Account\n", terminator: "")
////            UserDefaults.standard.setValue("OUR SHOP",forKey:"Catagoires_title")
////            self.openViewControllerBasedOnIdentifier("HomeViewController")
//
//            break
            
//        case 4:
//            print("account\n", terminator: "")
            
//            break
        case 2:
            print("Shop By Categories\n", terminator: "")
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier:"CategoriesViewController") as! CategoriesViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            break
        case 3:
            print("Login\n", terminator: "")
            if (UserDefaults.standard.object(forKey: "user_id") != nil) {
              //self.displayAlertWithMessage(viewController: self, title: "Logout", message: "Do you want to logout?")
                if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                    self.userLogout(token: access_token)
                }
            }
            else {
                if let viewControllers = self.navigationController?.viewControllers {
                    for vc in viewControllers {
                        if vc.isKind(of: LoginViewController.classForCoder()) {
                            self.navigationController!.popToViewController(vc, animated: false)
                            break
                            //Your Process
                        }
                        else {
                            let storyboard = UIStoryboard(name:"Main", bundle: nil)
                            let vc  = storyboard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
                else {
                   
                }
               
            }
           
            
            break
            
            default:
            print("default\n", terminator: "")
        }
    }
    
    //MARK: - customer's Login Out API call
    func userLogout(token:String) {
       
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + token
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/logout", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                    {
                        
                        if let success = dictData["success"]
                        {
                            
                            if success.boolValue {
                                UserDefaults.standard.removeObject(forKey: "user_id")
                                UserDefaults.standard.removeObject(forKey: "cartCount")
                                UserDefaults.standard.removeObject(forKey: "access_token")
                                UserDefaults.standard.removeObject(forKey: "productsInCart")
                                if let viewControllers = self.navigationController?.viewControllers {
                                    for vc in viewControllers {
                                        if vc.isKind(of: LoginViewController.classForCoder()) {
                                            self.navigationController!.popToViewController(vc, animated: false)
                                            break
                                            //Your Process
                                        }
                                        else {
                                            let storyboard = UIStoryboard(name:"Main", bundle: nil)
                                            let vc  = storyboard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                                else {
                                   
                                }
                                
                            }
                            else{
                                self.displayAlertWithMessage(viewController: self, title: "", message: "Unable to logout this time.")
                            }
                        }
                        
                    }
                    self.stopActivityIndicator()
                }
                break
                
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 400:
                        message = "Bad Request"
                    case 409:
                        message = "Conflict, Email Id is already registered"
                    case 500:
                        message = "Server Error!"
                    default:
                        message = (response.result.error?.localizedDescription)!
                    }
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: message)
                }
                else {
                    message = (response.result.error?.localizedDescription)!
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: "Error in server connection!")
                }
                break
                
            }
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
//        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
//            print("Same VC")
//        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
//            let storyboard = UIStoryboard(name:"Main", bundle: nil)
//            let vc  = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//            self.navigationController?.pushViewController(vc, animated: true)
       // }
    }
    
    func SharingMethod()
    {
        // text to share
        // let text = "This is some text that I want to share."
//        let name = "Name - " + lbl_title.text!
//        let address = "Address - " + self.address
        
        let text = "3m Connect"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }

    func addSlideMenuButton(){
        
        let btnShowMenu = UIButton(type:.custom)
        //btnShowMenu.setImage(self.defaultMenuImage(), for: UIControlState())
         btnShowMenu.setImage(UIImage.init(named: "menu"), for: UIControlState())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }

    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
       
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
    }
}
