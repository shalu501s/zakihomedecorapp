//
//  CartTotalTableViewCell.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 8/18/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit

class CartTotalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTotal: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
