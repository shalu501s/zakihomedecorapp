//
//  CategoriesTableViewCell.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 8/5/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {
    @IBOutlet weak var lblCategoryName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
