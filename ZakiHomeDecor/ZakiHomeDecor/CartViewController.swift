//
//  CartViewController.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 8/14/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class CartViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
     @IBOutlet weak var tblViewCart: UITableView!
    var arrProductData = [Dictionary<String,AnyObject>]()
    var arrTotals = [Dictionary<String,AnyObject>]()
    var numberOfSections = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "CART"
        if UserDefaults.standard.bool(forKey: "productsInCart")
        {
            if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                self.getCartList(strToken: access_token)
            }
        }
        else
        {
           self.displayAlertWithMessage(viewController: self, title: "", message: "Please add products to cart first!")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Product Cart API
    func getCartList(strToken:String) {
        self.arrProductData.removeAll()
        self.arrTotals.removeAll()
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + strToken
        reqheader["Content-Type"] = "application/json"
        let strURL = "http://zakihomedecor.com/api/rest/cart"
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let httpStatusCode = response.response?.statusCode {
                        print("httpStatusCode is... \(httpStatusCode)")
                        print(response.result.value as Any)
                        if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                        {
                            if let arrData = dictData["data"] as! Dictionary<String,AnyObject>?
                            {
                                if let arrProductData = arrData["products"] as! NSArray? {
                                    for obj in arrProductData {
                                        self.arrProductData.append(obj as! [String : AnyObject])
                                    }
                                    
                                }
                                if let arrTotalData = arrData["totals"] as! NSArray? {
                                    for obj in arrTotalData {
                                        self.arrTotals.append(obj as! [String : AnyObject])
                                    }
                                }
                                let cartCount = arrData["total_product_count"]
                                UserDefaults.standard.set(cartCount!, forKey: "cartCount")
                            }
                            
                        }
                        self.tblViewCart.reloadData()
                        self.stopActivityIndicator()
                        
                    }
                }
                break
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    //MARK: - update Cart Item API call
    func updateCartItem(dictCart: [String:String],token:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + token
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/cart", method: .put, parameters: dictCart, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                    {
                        
                        if let success = dictData["success"]
                        {
                            
                            if success.boolValue {
                                //total_product_count
                                //check for cart products
//                                if let arrData = dictData["data"] as! Dictionary<String,AnyObject>?
//                                {
//                                    let cartCount = arrData["total_product_count"]
//                                    UserDefaults.standard.set(cartCount!, forKey: "cartCount")
//                                }
//
//                                UserDefaults.standard.set(true, forKey: "productsInCart")
                               
                                self.getCartList(strToken: token)
                                self.displayAlertWithMessage(viewController: self, title: "", message: "Cart updated successfully")
                                
                            }
//                            else {
//                                UserDefaults.standard.set(false, forKey: "productsInCart")
//                            }
                            
                        }
                        
                    }
                    self.tblViewCart.reloadData()
                    self.stopActivityIndicator()
                }
                break
                
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 400:
                        message = "Bad Request"
                    case 409:
                        message = "Conflict, Email Id is already registered"
                    case 500:
                        message = "Server Error!"
                    default:
                        message = (response.result.error?.localizedDescription)!
                    }
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: message)
                }
                else {
                    message = (response.result.error?.localizedDescription)!
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: "Error in server connection!")
                }
                break
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func decreaseQyantityTapped(_ sender: UIButton) {
        print("decrease")
        let obj = self.arrProductData[sender.tag]
        let strQyantity = obj["quantity"] as! String
        let product_id = obj["product_id"] as! String
        let intQ = Int(strQyantity)
        if intQ! > 1 {
            var dictCart = [String:String]()
            let newQ = intQ! - 1
            dictCart["quantity"] = "\(newQ)"
            dictCart["key"] = "\(product_id)"
            if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                self.updateCartItem(dictCart: dictCart, token: access_token)
            }
            
        }
        else {
            
        }
//        {
//            "key": "120",
//            "quantity": "2"
//        }
    }
    @IBAction func increaseQyantityTapped(_ sender: UIButton) {
        print("increase")
        let obj = self.arrProductData[sender.tag]
        let strQyantity = obj["quantity"] as! String
        let product_id = obj["product_id"] as! String
        let intQ = Int(strQyantity)
        if intQ! >= 1 {
            var dictCart = [String:String]()
            let newQ = intQ! + 1
            dictCart["quantity"] = "\(newQ)"
            dictCart["key"] = "\(product_id)"
            if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                self.updateCartItem(dictCart: dictCart, token: access_token)
            }
            
        }
    }
    // MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
           return 140
        }
        else {
            return 40
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrProductData.count
        }
        else {
            return arrTotals.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as! CartTableViewCell
            cell.selectionStyle = .none
            if arrProductData.count > 0 {
                let obj = arrProductData[indexPath.row]
                let strName = obj["name"]!
                cell.lblName.text = "\(strName)"
                let priceGet = obj["price"]!
                let price = "\(priceGet)"
                let strQyantity = obj["quantity"]!
                cell.txtQuantity.text = "\(strQyantity)"
                cell.lblPrice.text = price
                let imgURL = URL(string:obj["thumb"] as! String)
                cell.imgProd?.kf.setImage(with:imgURL)
                
                cell.btnPlus.tag = indexPath.row
                cell.btnMinus.tag = indexPath.row
                cell.btnPlus.addTarget(self, action: #selector(increaseQyantityTapped(_:)), for: .touchUpInside)
                cell.btnMinus.addTarget(self, action: #selector(decreaseQyantityTapped(_:)), for: .touchUpInside)
            }
            return cell
        }
        else {
             let totalcell = tableView.dequeueReusableCell(withIdentifier: "CartTotalTableViewCell", for: indexPath) as! CartTotalTableViewCell
            totalcell.selectionStyle = .none
            if arrTotals.count > 0 {
                let obj = arrTotals[indexPath.row]
                let strTotalTitle = obj["title"]!
                let strTotalValue = obj["text"]!
                totalcell.lblTitle.text = "\(strTotalTitle)"
                totalcell.lblTotal.text = "\(strTotalValue)"
            }
            return totalcell
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let objDoc = arrCat[indexPath.section]
//        let strCatID = objDoc["category_id"]!
//        let strCatName = objDoc["name"]!
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductsByCategoryViewController") as! ProductsByCategoryViewController
//        vc.cat_id = "\(strCatID)"
//        vc.CatName = "\(strCatName)"
//        self.navigationController?.pushViewController(vc, animated: true)
    }


}
