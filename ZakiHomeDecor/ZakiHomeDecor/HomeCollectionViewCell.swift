//
//  HomeCollectionViewCell.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 7/28/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
}
