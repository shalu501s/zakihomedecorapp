//
//  LoginViewController.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 7/28/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController {
    var activityIndicatorView: ActivityIndicatorView!
    
    @IBOutlet weak var textUserName: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    //Variables
    var dictLogin = [String:String]()
    var strUserId:String?
    var strAccessToken:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
//        self.getAccessToken()
//        UserDefaults.standard.removeObject(forKey: "productsInCart")
//        UserDefaults.standard.removeObject(forKey: "carCount")
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        //self.navigationController?.navigationBar.isHidden = true
        
        //UserDefaults.standard.removeObject(forKey: "productsInCart")
        //UserDefaults.standard.removeObject(forKey: "user_id")
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(false)
        //self.navigationController?.navigationBar.isHidden = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-  Access Token Api Call
    func getAccessToken(){
        let strToken = "Basic" + " c2hvcHBpbmdfb2F1dGhfY2xpZW50OnNob3BwaW5nX29hdXRoX3NlY3JldA"
        let headerValue: [String: String] = ["Authorization":strToken]
        
        Alamofire.request("http://zakihomedecor.com/api/rest/oauth2/token/client_credentials", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headerValue).responseJSON { (response:DataResponse<Any>) in
            
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    
                    if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                    {
                        
                        if let success = dictData["success"]
                        {
                            
                            if success.boolValue {
                                
                                let data = dictData["data"] as! NSDictionary
                                
                                UserDefaults.standard.setValue(data["access_token"]!, forKey: "access_token")
                                
                            }
                        }
                        
                    }
                }
                break
                
            case .failure(_):
                
                print(response.result.error as Any)
                
                break
                
            }
            
            
        }
        
    }
    //MARK: - customer's Login API call
    func userLogin(dictRegister: [String:String],token:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + token
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/login", method: .post, parameters: dictRegister, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                    {
                        
                        if let success = dictData["success"]
                        {
                            
                            if success.boolValue {
                                
                                let data = dictData["data"] as! NSDictionary
                                UserDefaults.standard.setValue(data["customer_id"]!, forKey: "user_id")
                                self.stopActivityIndicator()
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                            else{
                                if let erroMSg = dictData["error"] {
                                  self.displayAlertWithMessage(viewController: self, title: "", message: "\(erroMSg)")
                                }
                               
                            }
                        }
                        
                    }
                    self.stopActivityIndicator()
                }
                break
                
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                let message : String
                if let httpStatusCode = response.response?.statusCode {
                    switch(httpStatusCode) {
                    case 400:
                        message = "Bad Request"
                    case 409:
                        message = "Conflict, Email Id is already registered"
                    case 500:
                        message = "Server Error!"
                    default:
                        message = (response.result.error?.localizedDescription)!
                    }
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: message)
                }
                else {
                    message = (response.result.error?.localizedDescription)!
                    self.displayAlertWithMessage(viewController: self, title: "Error!", message: "Error in server connection!")
                }
                break
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK:- Sign In Action
    @IBAction func signInAction(_ sender: Any) {
        let strEmail = textUserName.text
        let strPassword = textPassword.text
        
        //Validation
        if !isValidEmail(testStr: strEmail!) == true {
            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Please enter a valid email Id!")
        }
        else if (strPassword?.characters.count)! <= 4 {
            self.displayAlertWithMessage(viewController: self, title: "Alert", message: "Please enter a 5 digit password!")
        }
        else {
            dictLogin["email"] = strEmail
            dictLogin["password"] = strPassword
            if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                strAccessToken = access_token
            }
            self.userLogin(dictRegister: dictLogin, token: strAccessToken!)
    }
    }
    
    @IBAction func forgetPWPressed(_ sender: Any) {
    }
    
    @IBAction func registerPressed(_ sender: Any) {
    }
    
    //MARK: - UTIL
    //Validate Email
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}
