//
//  HomeViewController.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 7/28/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class HomeViewController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionViewLatest: UICollectionView!
    @IBOutlet weak var collectionViewBest: UICollectionView!
    @IBOutlet weak var collectionSpecial: UICollectionView!
    
    @IBOutlet weak var rightBarButton: BadgedBarButtonItem!
    var scrollview_pagecontrol: UIScrollView?
    
    @IBOutlet weak var mainViewHight: NSLayoutConstraint!
    @IBOutlet var view_back_scroll: UIView?
    @IBOutlet var mainView: UIView?
    @IBOutlet  var pageControl: UIPageControl?
    var arrProductImg = NSMutableArray()
    var arrSpecialProductData = [Dictionary<String,AnyObject>]()
    var arrBestSellersProductData = [Dictionary<String,AnyObject>]()
    var arrLatestProductData = [Dictionary<String,AnyObject>]()
    @IBOutlet weak var heightCVSpecial: NSLayoutConstraint!
    @IBOutlet weak var heightCVLatest: NSLayoutConstraint!
    @IBOutlet weak var heightCVBest: NSLayoutConstraint!
    fileprivate var rightCount = 0
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "HOME"
        self.navigationController?.navigationBar.isHidden = false
            timer = Timer.scheduledTimer(timeInterval: 120, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
        addSlideMenuButton()
        self.arrProductImg = ["slide1","slide2","slide3","slide4"]
        self.createPageControl()
        if UserDefaults.standard.object(forKey: "access_token") == nil {
         self.getAccessToken()
        }
        else {
            if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                self.getLatestProductList(strToken: access_token)
                self.getSpecialProductList(strToken: access_token)
                self.getBestSellersProductList(strToken: access_token)
            }
        }
       
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        super.viewWillAppear(false)
       
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        if let cartCount = UserDefaults.standard.object(forKey: "cartCount") as? Int {
            rightCount = cartCount
        }
        //Cart Badge
        let image = UIImage(imageLiteralResourceName: "cart")
        let buttonFrame: CGRect = CGRect(x: -10, y: 0.0, width: image.size.width, height: image.size.height)
        let barButton = BadgedBarButtonItem(
            startingBadgeValue: 0,
            frame: buttonFrame,
            image: image
        )
        barButton.tintColor = UIColor.white
        barButton.badgeValue = rightCount
        //sender.badgeValue = rightCount
        rightBarButton = barButton
        rightBarButton?.addTarget(self, action: #selector(rightBarButtonTapped(_:)))
        navigationItem.rightBarButtonItem = rightBarButton
        
        timer.invalidate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func runTimedCode() {
        UserDefaults.standard.removeObject(forKey: "cartCount")
        UserDefaults.standard.removeObject(forKey: "productsInCart")
    }
    
    //MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionSpecial {
            return arrSpecialProductData.count
        }
        else if collectionView == self.collectionViewLatest {
            return arrLatestProductData.count
        }
        else {
            return arrBestSellersProductData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        if collectionView == collectionSpecial {
            if arrSpecialProductData.count > 0{
                
                            let obj = arrSpecialProductData[indexPath.row]
                
                            cell.lblName.text = obj["name"]! as? String
                
                            let priceGet = obj["price"]!
                            let priceString = "RS. "
                            let price = "\(priceString)\(priceGet)"
                
                            cell.lblPrice.text = price
                            let imgURL = URL(string:obj["thumb"] as! String)
                            cell.imgProduct?.kf.setImage(with:imgURL)
            }

            
        }
        else if collectionView == collectionViewLatest {
                if arrLatestProductData.count > 0{
                    
                    let obj = arrLatestProductData[indexPath.row]
                    
                    cell.lblName.text = obj["name"]! as? String
                    
                    let priceGet = obj["price"]!
                    let priceString = "RS. "
                    let price = "\(priceString)\(priceGet)"
                    
                    cell.lblPrice.text = price
                    let imgURL = URL(string:obj["thumb"] as! String)
                    cell.imgProduct?.kf.setImage(with:imgURL)
                    
                    
                }
            
        }
        else {
                if arrBestSellersProductData.count > 0{
                    
                    let obj = arrBestSellersProductData[indexPath.row]
                    
                    cell.lblName.text = obj["name"]! as? String
                    
                    let priceGet = obj["price"]!
                    let priceString = "RS. "
                    let price = "\(priceString)\(priceGet)"
                    
                    cell.lblPrice.text = price
                    let imgURL = URL(string:obj["thumb"] as! String)
                    cell.imgProduct?.kf.setImage(with:imgURL)
                    
                    
                }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var screenWidth: CGFloat!
        //var screenHeight: CGFloat!
        screenWidth = self.collectionViewLatest.frame.size.width
        // screenHeight = self.view_bckCollection.frame.size.height
        return CGSize(width: 180, height: 212)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var strProductID:Any!
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        
        if collectionView == self.collectionSpecial {
            if arrSpecialProductData.count > 0 {
                let obj = arrSpecialProductData[indexPath.row]
                strProductID = obj["product_id"]!
            }
        }
        else if collectionView == self.collectionViewLatest {
            if arrLatestProductData.count > 0 {
                let obj = arrLatestProductData[indexPath.row]
                strProductID = obj["product_id"]!
            }
        }
        else {
            if arrBestSellersProductData.count > 0 {
                let obj = arrBestSellersProductData[indexPath.row]
                strProductID = obj["product_id"]!
            }
        }
        if strProductID != nil {
            vc.product_id = "\(strProductID!)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            
        }
       
        
    }
    //MARK:-  Access Token Api Call
    func getAccessToken(){
        let strToken = "Basic" + " c2hvcHBpbmdfb2F1dGhfY2xpZW50OnNob3BwaW5nX29hdXRoX3NlY3JldA"
        let headerValue: [String: String] = ["Authorization":strToken]
        
        Alamofire.request("http://zakihomedecor.com/api/rest/oauth2/token/client_credentials", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headerValue).responseJSON { (response:DataResponse<Any>) in
            
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    
                    if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                    {
                        
                        if let success = dictData["success"]
                        {
                            
                            if success.boolValue {
                                
                                let data = dictData["data"] as! NSDictionary
                                
                                UserDefaults.standard.setValue(data["access_token"]!, forKey: "access_token")
                                if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                                    self.getLatestProductList(strToken: access_token)
                                    self.getSpecialProductList(strToken: access_token)
                                    self.getBestSellersProductList(strToken: access_token)
                                }
                                
                            }
                        }
                        
                    }
                }
                break
                
            case .failure(_):
                
                print(response.result.error as Any)
                
                break
                
            }
            
            
        }
        
    }
    
    //MARK: - Special Product list API
    func getSpecialProductList(strToken:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + strToken
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/specials", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let httpStatusCode = response.response?.statusCode {
                        print("httpStatusCode is... \(httpStatusCode)")
                        print(response.result.value as Any)
                        if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                        {
                            if let arrData = dictData["data"] as! NSArray? {
                                if arrData.count > 0 {
                                    for obj in arrData {
                                        self.arrSpecialProductData.append(obj as! [String : AnyObject])
                                    }
                                }
                                else {
                                    self.heightCVSpecial.constant = 0
                                }
                                
                            }
                        }
                        self.collectionSpecial.reloadData()
                        self.stopActivityIndicator()
                        
                    }
                }
                break
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    //MARK: - Latest Product list API
    func getLatestProductList(strToken:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + strToken
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/latest", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let httpStatusCode = response.response?.statusCode {
                        print("httpStatusCode is... \(httpStatusCode)")
                        print(response.result.value as Any)
                        if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                        {
                            if let arrData = dictData["data"] as! NSArray? {
                                if arrData.count > 0 {
                                    for obj in arrData {
                                        self.arrLatestProductData.append(obj as! [String : AnyObject])
                                    }
                                }
                                else {
                                    self.heightCVLatest.constant = 0
                                }
                                
                            }
                        }
                        self.collectionViewLatest.reloadData()
                        self.stopActivityIndicator()
                        
                    }
                }
                break
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    //MARK: - Best Sellers Product list API
    func getBestSellersProductList(strToken:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + strToken
        reqheader["Content-Type"] = "application/json"
        Alamofire.request("http://zakihomedecor.com/api/rest/bestsellers", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let httpStatusCode = response.response?.statusCode {
                        print("httpStatusCode is... \(httpStatusCode)")
                        print(response.result.value as Any)
                        if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                        {
                            if let arrData = dictData["data"] as! NSArray? {
                                if arrData.count > 0 {
                                    for obj in arrData {
                                        self.arrBestSellersProductData.append(obj as! [String : AnyObject])
                                    }
                                }
                                else {
                                    self.heightCVBest.constant = 0
                                }
                                
                            }
                        }
                        self.collectionViewBest.reloadData()
                        self.stopActivityIndicator()
                        
                    }
                }
                break
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func rightBarButtonTapped(_ sender: BadgedBarButtonItem) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"CartViewController") as! CartViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: -  Page Control
    func createPageControl() {
        scrollview_pagecontrol = UIScrollView(frame: CGRect(x: CGFloat((view_back_scroll?.frame.origin.x)!), y: 0, width: CGFloat((view_back_scroll?.frame.size.width)!), height: CGFloat((self.view_back_scroll?.frame.size.height)!)))
        scrollview_pagecontrol?.showsHorizontalScrollIndicator = false
        scrollview_pagecontrol?.delegate = self
        scrollview_pagecontrol?.bounces = false
        scrollview_pagecontrol?.tag = 1
        self.view_back_scroll?.addSubview(scrollview_pagecontrol!)
        pageControl?.pageIndicatorTintColor = UIColor.lightGray
        pageControl?.numberOfPages = arrProductImg.count
        pageControl?.currentPage = 0
        pageControl?.currentPageIndicatorTintColor = UIColor.blue
        pageControl?.tag = 12
        
        for i in 0..<arrProductImg.count {
            //We'll create an imageView object in every 'page' of our scrollView.
            var frame = CGRect.zero
            frame.origin.x = view.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size = view.frame.size
            frame.size.height = (scrollview_pagecontrol?.frame.size.height)!
            let imageView = UIImageView(frame: frame)
            let images: String? = (arrProductImg[i] as? String)
            DispatchQueue.main.async {
                //let imgURL = URL(string:images!)
                imageView.image = UIImage(named: images!)
                //imageView.kf.setImage(with:imgURL)
            }
            
            scrollview_pagecontrol?.addSubview(imageView)
            scrollview_pagecontrol?.isPagingEnabled = true
            
            
            scrollview_pagecontrol?.contentSize = CGSize(width: CGFloat((scrollview_pagecontrol?.frame.size.width)! * CGFloat( arrProductImg.count)), height: CGFloat((scrollview_pagecontrol?.frame.size.height)!))
            
        }
    }
    
    // MARK: - UIScrollView Delegate
    func scrollViewDidScroll(_ sender: UIScrollView) {
        
        let pageWidth: CGFloat = scrollview_pagecontrol!.frame.size.width
        let Page: CGFloat = floor((scrollview_pagecontrol!.contentOffset.x - CGFloat(pageWidth / 2)) / CGFloat(pageWidth)) + CGFloat(1)
        pageControl?.currentPage = Int(Page)
    }

}
