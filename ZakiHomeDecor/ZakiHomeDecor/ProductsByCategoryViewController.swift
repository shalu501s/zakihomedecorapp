//
//  ProductsByCategoryViewController.swift
//  ZakiHomeDecor
//
//  Created by Shalu on 8/7/18.
//  Copyright © 2018 Shalu. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class ProductsByCategoryViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    var arrProductData = [Dictionary<String,AnyObject>]()
    var cat_id:String!
    var CatName:String!
    var page_no = 0
    var isMoreDataAvailable = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
            self.getProductList(strToken: access_token)
        }
        self.title = "\(CatName!)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Product list API
    func getProductList(strToken:String) {
        self.startActivityIndicator()
        var reqheader = [String:String]()
        reqheader["Authorization"] = "Bearer " + strToken
        reqheader["Content-Type"] = "application/json"
        let strURL = "http://zakihomedecor.com/api/rest/products/category/" + "\(self.cat_id!)" + "/limit/10/page/" + "\(page_no)"
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: reqheader).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    print(response.result.value as Any)
                    if let httpStatusCode = response.response?.statusCode {
                        print("httpStatusCode is... \(httpStatusCode)")
                        print(response.result.value as Any)
                        if let dictData = response.result.value as! Dictionary<String,AnyObject>?
                        {
                            if let arrData = dictData["data"] as! NSArray? {
                                if arrData.count > 0 {
                                    for obj in arrData {
                                        self.arrProductData.append(obj as! [String : AnyObject])
                                    }
                                    self.isMoreDataAvailable = true
                                    self.page_no += 1
                                }
                                else{
                                    self.isMoreDataAvailable = false
                                }
                              
                            }
                        }
                        self.collectionView.reloadData()
                        self.stopActivityIndicator()
                        
                    }
                }
                break
                
            case .failure(_):
                self.stopActivityIndicator()
                print(response.result.error as Any)
                break
                
            }
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - get Product by Categories
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProductData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        
        if arrProductData.count > 0 {

            let obj = arrProductData[indexPath.row]

            cell.lblName.text = obj["name"] as? String

            let priceGet = obj["price"]!
            let priceString = "Rs. "
            let price = "\(priceString)\(priceGet)"

            cell.lblPrice.text = price
            let imgURL = URL(string:obj["image"] as! String)
            cell.imgProduct?.kf.setImage(with:imgURL)

        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var screenWidth: CGFloat!
        //var screenHeight: CGFloat!
        screenWidth = self.collectionView.frame.size.width
        // screenHeight = self.view_bckCollection.frame.size.height
        return CGSize(width: screenWidth/2-5, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let obj = arrProductData[indexPath.row]
        
        let strProdID = obj["product_id"]!
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
        vc?.product_id = "\(strProdID)"
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            if(indexPath.section == (self.arrProductData.count-1)) {
                if isMoreDataAvailable {
                    if let access_token = UserDefaults.standard.object(forKey: "access_token") as? String {
                        self.getProductList(strToken: access_token)
                    }
                }
            }

    }

}
